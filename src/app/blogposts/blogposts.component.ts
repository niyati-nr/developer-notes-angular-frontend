import { Component, OnInit } from '@angular/core';
import {BlogPost} from '../blogpost';
import {BlogPosts} from '../mock-blogposts';
import {BlogpostService} from '../blogpost.service';
@Component({
  selector: 'app-blogposts',
  templateUrl: './blogposts.component.html',
  styleUrls: ['./blogposts.component.css']
})
export class BlogpostsComponent implements OnInit {
  blogPosts:BlogPost[] = [];
  constructor(private blogpostService:BlogpostService) { 

  }

  ngOnInit(): void {
    this.getBlogs();
  }

  getBlogs(): void {
    this.blogpostService.getBlogPosts()
    .subscribe(blogPosts => this.blogPosts=blogPosts);
  }

  deletePost(blog:BlogPost, id:string):void{
    if(window.confirm('Are you sure you want to delete this post?')){
    this.blogPosts = this.blogPosts.filter(h => h !== blog);
    this.blogpostService.deletePost(id).subscribe();
    }
  }
  
}
