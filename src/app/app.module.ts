import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BlogpostsComponent } from './blogposts/blogposts.component';
import { FormsModule } from '@angular/forms';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { AppRoutingModule } from './app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { nl2brPipe } from './nl2br.pipe';
import { HttpClientModule } from '@angular/common/http';
import { BlogPostEditComponent } from './blog-post-edit/blog-post-edit.component';
import { BlogPostAddComponent } from './blog-post-add/blog-post-add.component';
import { CarouselDisplayComponent } from './carousel-display/carousel-display.component';

@NgModule({
  declarations: [
    AppComponent,
    BlogpostsComponent,
    BlogDetailComponent,
    DashboardComponent,
    BlogPostEditComponent,
    BlogPostAddComponent,
    nl2brPipe,
    CarouselDisplayComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
