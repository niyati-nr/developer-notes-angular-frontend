import { Component, OnInit, Input } from '@angular/core';
import {BlogPost} from '../blogpost';
import {BlogpostService} from '../blogpost.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'app-blog-post-edit',
  templateUrl: './blog-post-edit.component.html',
  styleUrls: ['./blog-post-edit.component.css']
})
export class BlogPostEditComponent implements OnInit {
  @Input() blog?: BlogPost;
  constructor(
    private route: ActivatedRoute,
    private blogpostService: BlogpostService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getBlogPost();
  }
  getBlogPost(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.blogpostService.getBlogPost(id)
      .subscribe(blogPost => this.blog = blogPost);
  }
  savePost(): void{
    const id = this.route.snapshot.paramMap.get('id');
    this.blogpostService.updatePost(this.blog,id)
    .subscribe(() => this.goBack());
  }
  goBack(): void {
    this.location.back();
  }
}
