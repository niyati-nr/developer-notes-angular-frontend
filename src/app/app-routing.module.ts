import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlogpostsComponent } from './blogposts/blogposts.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {BlogDetailComponent} from './blog-detail/blog-detail.component';
import {BlogPostEditComponent} from './blog-post-edit/blog-post-edit.component';
import {BlogPostAddComponent} from './blog-post-add/blog-post-add.component';
import { BlogPosts } from './mock-blogposts';
const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'blogposts', component: BlogpostsComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'detail/:id', component: BlogDetailComponent },
  { path: 'edit/:id', component: BlogPostEditComponent },
  { path: 'addpost', component: BlogPostAddComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }