import { Component, OnInit, Input } from '@angular/core';
import {BlogPost} from '../blogpost';
import {BlogpostService} from '../blogpost.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { BuiltinFunctionCall } from '@angular/compiler/src/compiler_util/expression_converter';
@Component({
  selector: 'app-blog-post-add',
  templateUrl: './blog-post-add.component.html',
  styleUrls: ['./blog-post-add.component.css']
})
export class BlogPostAddComponent implements OnInit {
  @Input() blog?: BlogPost;
  constructor(
    private route: ActivatedRoute,
    private blogpostService: BlogpostService,
    private location: Location
  ) { }

  ngOnInit(): void {
  }
  publishPost(f): void{
    var blog=<BlogPost>{};
    blog.title = f.value.title;
    blog.author = f.value.author;
    blog.categories = f.value.categories.split(",");
    blog.content = f.value.content;
    // console.log(f.value);
    this.blogpostService.publishPost(blog)
    .subscribe(() => this.goBack());
  }
  goBack(): void {
    this.location.back();
  }
}
