import { Component, OnInit } from '@angular/core';
import { BlogPost } from '../blogpost';
import { BlogpostService } from '../blogpost.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {
  blogPosts: BlogPost[] = [];

  constructor(private blogpostService: BlogpostService) { }

  ngOnInit() {
    this.getBlogPosts();
  }

  getBlogPosts(): void {
    this.blogpostService.getBlogPosts()
      .subscribe(blogPosts => this.blogPosts = blogPosts.slice(0, 5));
  }
}