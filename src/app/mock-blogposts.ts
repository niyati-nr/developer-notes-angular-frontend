import {BlogPost} from './blogpost';

export const BlogPosts:BlogPost[]=[
    {
        "categories": [
          "tutorial",
          "mongoose",
          "general",
          "Node js",
          "Web Development"
      ],
      "authorid": "anonymous",
      "bloguserid": "anonymous",
      "id": "6066ca1c7ebc960708bc8c77",
      "title": "Node.js Tutorial",
      "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur fringilla tellus vitae lectus sollicitudin, et consequat nibh ullamcorper. Morbi non vestibulum tellus. Proin non dui eget magna pretium malesuada sed at ante. Ut hendrerit orci metus, in faucibus nisl suscipit in. In vulputate, odio nec vulputate pharetra, libero magna viverra ante, id pellentesque nunc augue non magna. Fusce ac porttitor turpis, sed porta sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam ligula libero, lobortis ac neque sed, sodales finibus elit. Curabitur sit amet diam at nisl semper viverra vitae a purus. Suspendisse porta vitae risus in finibus. Vestibulum interdum vel est quis maximus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus tincidunt risus turpis, at condimentum nibh lobortis non. Aliquam pellentesque eleifend molestie. Donec a dolor quis dolor bibendum scelerisque sit amet id ante. Suspendisse volutpat urna at urna fringilla, vel mollis elit viverra. Aliquam a ipsum feugiat, aliquam ligula auctor, molestie massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nulla facilisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Suspendisse congue id odio sit amet fermentum.",
      "author": "Laxmi Rana",
      "date": Date.now()
      },
      {
        "categories": [
            "tutorial",
            "mongoose",
            "general"
        ],
        "authorid": "anonymous",
        "bloguserid": "anonymous",
        "id": "6066ca3b7ebc960708bc8c78",
        "title": "Mongoose Tutorial",
        "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur fringilla tellus vitae lectus sollicitudin, et consequat nibh ullamcorper. Morbi non vestibulum tellus. Proin non dui eget magna pretium malesuada sed at ante. Ut hendrerit orci metus, in faucibus nisl suscipit in. In vulputate, odio nec vulputate pharetra, libero magna viverra ante, id pellentesque nunc augue non magna. Fusce ac porttitor turpis, sed porta sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam ligula libero, lobortis ac neque sed, sodales finibus elit. Curabitur sit amet diam at nisl semper viverra vitae a purus. Suspendisse porta vitae risus in finibus. Vestibulum interdum vel est quis maximus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus tincidunt risus turpis, at condimentum nibh lobortis non. Aliquam pellentesque eleifend molestie. Donec a dolor quis dolor bibendum scelerisque sit amet id ante. Suspendisse volutpat urna at urna fringilla, vel mollis elit viverra. Aliquam a ipsum feugiat, aliquam ligula auctor, molestie massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nulla facilisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Suspendisse congue id odio sit amet fermentum.",
        "author": "Nikhil Rana",
        "date": Date.now()
      },
        {
            "categories": [
                "tutorial",
                "mongoose",
                "general",
                "sass",
                "css"
            ],
            "authorid": "anonymous",
            "bloguserid": "anonymous",
            "id": "6066ca677ebc960708bc8c79",
            "title": "Sass Tutorial",
            "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur fringilla tellus vitae lectus sollicitudin, et consequat nibh ullamcorper. Morbi non vestibulum tellus. Proin non dui eget magna pretium malesuada sed at ante. Ut hendrerit orci metus, in faucibus nisl suscipit in. In vulputate, odio nec vulputate pharetra, libero magna viverra ante, id pellentesque nunc augue non magna. Fusce ac porttitor turpis, sed porta sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam ligula libero, lobortis ac neque sed, sodales finibus elit. Curabitur sit amet diam at nisl semper viverra vitae a purus. Suspendisse porta vitae risus in finibus. Vestibulum interdum vel est quis maximus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus tincidunt risus turpis, at condimentum nibh lobortis non. Aliquam pellentesque eleifend molestie. Donec a dolor quis dolor bibendum scelerisque sit amet id ante. Suspendisse volutpat urna at urna fringilla, vel mollis elit viverra. Aliquam a ipsum feugiat, aliquam ligula auctor, molestie massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nulla facilisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Suspendisse congue id odio sit amet fermentum.",
            "author": "Nikhil Rana",
            "date": Date.now()
        },
        {
            "categories": [
                "tutorial",
                "mongoose",
                "general",
                "sass",
                "css"
            ],
            "authorid": "anonymous",
            "bloguserid": "anonymous",
            "id": "6066ca857ebc960708bc8c7a",
            "title": "HTML/CSS/JS Tutorial",
            "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur fringilla tellus vitae lectus sollicitudin, et consequat nibh ullamcorper. Morbi non vestibulum tellus. Proin non dui eget magna pretium malesuada sed at ante. Ut hendrerit orci metus, in faucibus nisl suscipit in. In vulputate, odio nec vulputate pharetra, libero magna viverra ante, id pellentesque nunc augue non magna. Fusce ac porttitor turpis, sed porta sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam ligula libero, lobortis ac neque sed, sodales finibus elit. Curabitur sit amet diam at nisl semper viverra vitae a purus. Suspendisse porta vitae risus in finibus. Vestibulum interdum vel est quis maximus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus tincidunt risus turpis, at condimentum nibh lobortis non. Aliquam pellentesque eleifend molestie. Donec a dolor quis dolor bibendum scelerisque sit amet id ante. Suspendisse volutpat urna at urna fringilla, vel mollis elit viverra. Aliquam a ipsum feugiat, aliquam ligula auctor, molestie massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nulla facilisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Suspendisse congue id odio sit amet fermentum.",
            "author": "Nikhil Rana",
            "date": Date.now()
      },
      {
        "categories": [
          "tutorial",
          "mongoose",
          "general",
          "Node js",
          "Web Development"
      ],
      "authorid": "anonymous",
      "bloguserid": "anonymous",
      "id": "6066ca1c7ebc960708bc8c77",
      "title": "Node.js Tutorial",
      "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur fringilla tellus vitae lectus sollicitudin, et consequat nibh ullamcorper. Morbi non vestibulum tellus. Proin non dui eget magna pretium malesuada sed at ante. Ut hendrerit orci metus, in faucibus nisl suscipit in. In vulputate, odio nec vulputate pharetra, libero magna viverra ante, id pellentesque nunc augue non magna. Fusce ac porttitor turpis, sed porta sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam ligula libero, lobortis ac neque sed, sodales finibus elit. Curabitur sit amet diam at nisl semper viverra vitae a purus. Suspendisse porta vitae risus in finibus. Vestibulum interdum vel est quis maximus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus tincidunt risus turpis, at condimentum nibh lobortis non. Aliquam pellentesque eleifend molestie. Donec a dolor quis dolor bibendum scelerisque sit amet id ante. Suspendisse volutpat urna at urna fringilla, vel mollis elit viverra. Aliquam a ipsum feugiat, aliquam ligula auctor, molestie massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nulla facilisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Suspendisse congue id odio sit amet fermentum.",
      "author": "Laxmi Rana",
      "date": Date.now()
      },
      {
        "categories": [
          "tutorial",
          "mongoose",
          "general",
          "Node js",
          "Web Development"
      ],
      "authorid": "anonymous",
      "bloguserid": "anonymous",
      "id": "6066ca1c7ebc960708bc8c77",
      "title": "Node.js Tutorial",
      "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur fringilla tellus vitae lectus sollicitudin, et consequat nibh ullamcorper. Morbi non vestibulum tellus. Proin non dui eget magna pretium malesuada sed at ante. Ut hendrerit orci metus, in faucibus nisl suscipit in. In vulputate, odio nec vulputate pharetra, libero magna viverra ante, id pellentesque nunc augue non magna. Fusce ac porttitor turpis, sed porta sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam ligula libero, lobortis ac neque sed, sodales finibus elit. Curabitur sit amet diam at nisl semper viverra vitae a purus. Suspendisse porta vitae risus in finibus. Vestibulum interdum vel est quis maximus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus tincidunt risus turpis, at condimentum nibh lobortis non. Aliquam pellentesque eleifend molestie. Donec a dolor quis dolor bibendum scelerisque sit amet id ante. Suspendisse volutpat urna at urna fringilla, vel mollis elit viverra. Aliquam a ipsum feugiat, aliquam ligula auctor, molestie massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nulla facilisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Suspendisse congue id odio sit amet fermentum.",
      "author": "Laxmi Rana",
      "date": Date.now()
      },
      {
        "categories": [
          "tutorial",
          "mongoose",
          "general",
          "Node js",
          "Web Development"
      ],
      "authorid": "anonymous",
      "bloguserid": "anonymous",
      "id": "6066ca1c7ebc960708bc8c77",
      "title": "Node.js Tutorial",
      "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur fringilla tellus vitae lectus sollicitudin, et consequat nibh ullamcorper. Morbi non vestibulum tellus. Proin non dui eget magna pretium malesuada sed at ante. Ut hendrerit orci metus, in faucibus nisl suscipit in. In vulputate, odio nec vulputate pharetra, libero magna viverra ante, id pellentesque nunc augue non magna. Fusce ac porttitor turpis, sed porta sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam ligula libero, lobortis ac neque sed, sodales finibus elit. Curabitur sit amet diam at nisl semper viverra vitae a purus. Suspendisse porta vitae risus in finibus. Vestibulum interdum vel est quis maximus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus tincidunt risus turpis, at condimentum nibh lobortis non. Aliquam pellentesque eleifend molestie. Donec a dolor quis dolor bibendum scelerisque sit amet id ante. Suspendisse volutpat urna at urna fringilla, vel mollis elit viverra. Aliquam a ipsum feugiat, aliquam ligula auctor, molestie massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nulla facilisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Suspendisse congue id odio sit amet fermentum.",
      "author": "Laxmi Rana",
      "date": Date.now()
      },
      {
        "categories": [
          "tutorial",
          "mongoose",
          "general",
          "Node js",
          "Web Development"
      ],
      "authorid": "anonymous",
      "bloguserid": "anonymous",
      "id": "6066ca1c7ebc960708bc8c77",
      "title": "Node.js Tutorial",
      "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur fringilla tellus vitae lectus sollicitudin, et consequat nibh ullamcorper. Morbi non vestibulum tellus. Proin non dui eget magna pretium malesuada sed at ante. Ut hendrerit orci metus, in faucibus nisl suscipit in. In vulputate, odio nec vulputate pharetra, libero magna viverra ante, id pellentesque nunc augue non magna. Fusce ac porttitor turpis, sed porta sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam ligula libero, lobortis ac neque sed, sodales finibus elit. Curabitur sit amet diam at nisl semper viverra vitae a purus. Suspendisse porta vitae risus in finibus. Vestibulum interdum vel est quis maximus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus tincidunt risus turpis, at condimentum nibh lobortis non. Aliquam pellentesque eleifend molestie. Donec a dolor quis dolor bibendum scelerisque sit amet id ante. Suspendisse volutpat urna at urna fringilla, vel mollis elit viverra. Aliquam a ipsum feugiat, aliquam ligula auctor, molestie massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nulla facilisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Suspendisse congue id odio sit amet fermentum.",
      "author": "Laxmi Rana",
      "date": Date.now()
      },
      {
        "categories": [
          "tutorial",
          "mongoose",
          "general",
          "Node js",
          "Web Development"
      ],
      "authorid": "anonymous",
      "bloguserid": "anonymous",
      "id": "6066ca1c7ebc960708bc8c77",
      "title": "Node.js Tutorial",
      "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur fringilla tellus vitae lectus sollicitudin, et consequat nibh ullamcorper. Morbi non vestibulum tellus. Proin non dui eget magna pretium malesuada sed at ante. Ut hendrerit orci metus, in faucibus nisl suscipit in. In vulputate, odio nec vulputate pharetra, libero magna viverra ante, id pellentesque nunc augue non magna. Fusce ac porttitor turpis, sed porta sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam ligula libero, lobortis ac neque sed, sodales finibus elit. Curabitur sit amet diam at nisl semper viverra vitae a purus. Suspendisse porta vitae risus in finibus. Vestibulum interdum vel est quis maximus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus tincidunt risus turpis, at condimentum nibh lobortis non. Aliquam pellentesque eleifend molestie. Donec a dolor quis dolor bibendum scelerisque sit amet id ante. Suspendisse volutpat urna at urna fringilla, vel mollis elit viverra. Aliquam a ipsum feugiat, aliquam ligula auctor, molestie massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nulla facilisi. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Suspendisse congue id odio sit amet fermentum.",
      "author": "Laxmi Rana",
      "date": Date.now()
      }
]