import { Injectable } from '@angular/core';
import {BlogPost} from './blogpost';
import {BlogPosts} from './mock-blogposts';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class BlogpostService {
  constructor(
    private http: HttpClient
  ) { }
  private blogPostsUrl = '/devpost/';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  getBlogPosts(): Observable<BlogPost[]> {
    return this.http.get<BlogPost[]>(this.blogPostsUrl)
    .pipe(
      catchError(this.handleError<BlogPost[]>('getBlogPosts', []))
    );
  }
  getBlogPost(id: string): Observable<BlogPost> {
    const url = `${this.blogPostsUrl}/${id}`;
    return this.http.get<BlogPost>(url).pipe(
      catchError(this.handleError<BlogPost>(`getBlogPost id=${id}`))
    );
  }
  updatePost(blogPost:BlogPost, id:string):Observable<any>{
    const url = `${this.blogPostsUrl}/${id}`;
    return this.http.patch(url, blogPost, this.httpOptions).pipe(
      catchError(this.handleError<any>('updateBlogPost'))
    );
  }
  publishPost(blogPost:BlogPost):Observable<any>{
    return this.http.post(this.blogPostsUrl, blogPost, this.httpOptions).pipe(
      catchError(this.handleError<any>('updateBlogPost'))
    );
  }
  deletePost(id: string): Observable<BlogPost> {
    const url = `${this.blogPostsUrl}/${id}`;
    return this.http.delete<BlogPost>(url, this.httpOptions).pipe(
      catchError(this.handleError<BlogPost>('deleteBlogPost'))
    );
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
