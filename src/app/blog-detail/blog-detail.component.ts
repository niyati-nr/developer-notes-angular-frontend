import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { BlogpostService } from '../blogpost.service';
import {BlogPost} from '../blogpost';
@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.css']
})
export class BlogDetailComponent implements OnInit {
  @Input() blog?: BlogPost;
  id:string;
  blogDate:string;
  constructor(
    private route: ActivatedRoute,
    private blogpostService: BlogpostService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getBlogPost();
  }
  getBlogPost(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.blogpostService.getBlogPost(this.id)
      .subscribe(blogPost => {
        blogPost.content = blogPost.content.replace(/\n/g, "<br />");
        this.blog = blogPost
      });
  }
  deletePost(blog:BlogPost, id:string):void{
    if(window.confirm('Are you sure you want to delete this post?')){
    this.blogpostService.deletePost(id).subscribe();
    this.goBack();
    }
  }
  goBack(): void {
    this.location.back();
  }
}
