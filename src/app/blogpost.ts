export interface BlogPost {
    id?: string;
    title?: string;
    content?:string;
    date?:number;
    categories?:string[];
    author?:string;
    authorid?:string;
    bloguserid?:string;
  }